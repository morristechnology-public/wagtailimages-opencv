FROM lambci/lambda:build-python3.6

MAINTAINER Morris Technology <solutions@morristechnology.com>
LABEL description="WagtailImages OpenCV"

WORKDIR /usr/bin/
WORKDIR /home

RUN yum update -y
RUN yum install -y git cmake gcc-c++ gcc chrpath
RUN pip install numpy

RUN mkdir -p wagtailimages-opencv/cv2 build
WORKDIR build
RUN git clone https://github.com/Itseez/opencv.git
WORKDIR opencv
RUN git checkout 3.2.0

RUN cmake \
        # Only need core, objdetect, imgproc and ml - can disable all other modules
        -D BUILD_opencv_flann=OFF \
        -D BUILD_opencv_photo=OFF \
        -D BUILD_opencv_video=OFF \
        -D BUILD_opencv_imgcodecs=OFF \
        -D BUILD_opencv_shape=OFF \
        -D BUILD_opencv_videoio=OFF \
        -D BUILD_opencv_highgui=OFF \
        -D BUILD_opencv_superres=OFF \
        -D BUILD_opencv_features2d=OFF \
        -D BUILD_opencv_calib3d=OFF \
        -D BUILD_opencv_stitching=OFF \
        -D BUILD_opencv_videostab=OFF \
        # Enable python3 target
        -D BUILD_opencv_python3=ON \
        -D CMAKE_BUILD_TYPE=RELEASE \
        -D INSTALL_CREATE_DISTRIB=ON \
        -D BUILD_opencv_apps=OFF \
        -D BUILD_SHARED_LIBS=OFF \
        -D BUILD_EXAMPLES=OFF \
        -D BUILD_TESTS=OFF \
        -D BUILD_PERF_TESTS=OFF \
        -D PYTHON3_EXECUTABLE=$(which python) \
        -D PYTHON_DEFAULT_EXECUTABLE=$(which python) \
        -D python_EXECUTABLE=$(which python)  \
        -D PYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
        -D PYTHON_INCLUDE_DIR2=$(python -c "from os.path import dirname; from distutils.sysconfig import get_config_h_filename; print(dirname(get_config_h_filename()))")  \
        -D PYTHON_LIBRARY=$(python -c "from distutils.sysconfig import get_config_var;from os.path import dirname,join ; print(join(dirname(get_config_var('LIBPC')),get_config_var('LDLIBRARY')))")  \
        -D python_NUMPY_INCLUDE_DIRS=$(python -c "import numpy; print(numpy.get_include())")  \
        -D python_PACKAGES_PATH=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        .

RUN make -j`cat /proc/cpuinfo | grep MHz | wc -l`

WORKDIR /home
RUN cp build/opencv/lib/python3/cv2.cpython-36m-x86_64-linux-gnu.so wagtailimages-opencv/cv2/__init__.so

RUN strip --strip-all wagtailimages-opencv/cv2/*
RUN chrpath -r '$ORIGIN' wagtailimages-opencv/cv2/__init__.so
RUN touch wagtailimages-opencv/cv2/__init__.py

# Packge into wheel
RUN pip install setuptools wheel
WORKDIR /home/wagtailimages-opencv
COPY setup.py .
ARG package_name
ENV PACKAGE_NAME=$package_name
RUN python setup.py sdist bdist_wheel

WORKDIR /home
