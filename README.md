# Anvil
[![pipeline status](https://gitlab.com/morristechnology-public/wagtailimages-opencv/badges/master/pipeline.svg)](https://gitlab.com/morristechnology-public/wagtailimages-opencv/commits/master)

Stripped down OpenCV build for Python 3.6 to fulfill the minimum functional requirements for wagtailimages. Optimized size for use in AWS Lambda images.

## Usage

To install, pip install the job artifact wheel for the appropriate platform. 

Linux example:

```bash
pip install https://gitlab.com/api/v4/projects/10536909/jobs/artifacts/master/raw/wagtailimages_opencv-1.0-cp36-cp36m-linux_x86_64.whl?job=build%20linux
```