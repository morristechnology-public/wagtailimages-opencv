from os import environ
from setuptools import setup, Distribution


class BinaryDistribution(Distribution):

    @staticmethod
    def has_ext_modules():
        return True


setup(
    name=environ.get('PACKAGE_NAME', 'wagtailimages-opencv'),
    version='1.0',
    description='Stripped down OpenCV build to fulfill the minimum functional requirements for wagtailimages',
    packages=['cv2'],
    package_data={
        'cv2': [
            '__init__.so'
        ]
    },
    distclass=BinaryDistribution
)
